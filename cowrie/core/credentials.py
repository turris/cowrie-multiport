# Copyright (c) 2015 Michel Oosterhof <michel@oosterhof.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of the author(s) may not be used to endorse or promote
#    products derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

"""
This module contains ...
"""

import json
from zope.interface import implementer

from twisted.cred.credentials import IUsernamePassword, \
    ICredentials

class IUsername(ICredentials):
    """
    Encapsulate username only

    @type username: C{str}
    @ivar username: The username associated with these credentials.
    """



class IUsernamePasswordIP(IUsernamePassword):
    """
    I encapsulate a username, a plaintext password and a source IP

    @type username: C{str}
    @ivar username: The username associated with these credentials.

    @type password: C{str}
    @ivar password: The password associated with these credentials.

    @type ip: C{str}
    @ivar ip: The source ip address associated with these credentials.
    """



class IPluggableAuthenticationModulesIP(ICredentials):
    """
    Twisted removed IPAM in 15, adding in Cowrie now
    """



@implementer(IPluggableAuthenticationModulesIP)
class PluggableAuthenticationModulesIP(object):
    """
    Twisted removed IPAM in 15, adding in Cowrie now
    """

    def __init__(self, username, pamConversion, ip):
        self.username = username
        self.pamConversion = pamConversion
        self.ip = ip



@implementer(IUsername)
class Username(object):
    """
    """
    def __init__(self, username):
        self.username = username



@implementer(IUsernamePasswordIP)
class UsernamePasswordIP(object):
    """
    This credential interface also provides an IP address
    """

    @staticmethod
    def demangle_password(password):
        """Demangle password from jsonified in-band signaling from mitmproxy, e.g.
        {"pass": "zaq12wsx", "remote": "82.165.199.122", "remote_port": 52466, "turris_id": "00000005000002ee"}

        @returns tuple (password, remote, turris_id). Last two fields are None
                if it's not valid json.

                turris_id is not used in cowrie
        """
        try:
            d = json.loads(password)
            # in case there is no remote_port in json, last item will be None
            return (d.get("pass", ""), d.get("remote"), d.get("remote_port"))
        except ValueError:
            return (password, None, None)
        except AttributeError:
            return (password, None, None)


    def __init__(self, username, password, turrisIP):
        """
        Hack: this will try to de-mangle password from json format that is sent
        by mitmproxy.
        """
        self.username = username
        self.password, self.remoteIP, self.remotePort = self.demangle_password(password)
        self.ip = turrisIP
        # this attribute is only for database logging. So migration scripts
        # still migrate all present data.
        # TODO: store demangled attributes to several database columns
        self.json_password = password

