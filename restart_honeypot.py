#!/usr/bin/env python

"""
Restarter script, because that fucking tamagotchi won't quit eating RAM
"""

import sys
import os
import os.path
import time
import logging
import signal
import subprocess
import re

import smtplib
from email.mime.text import MIMEText
from ConfigParser import SafeConfigParser

cowrieDir = os.path.dirname(os.path.realpath(__file__))

cowriePidFile = cowrieDir + "/cowrie.pid"
logFile = cowrieDir + "/restart_honeypot.log"
stopScript = cowrieDir + "/stop.sh"
startScript = cowrieDir + "/start.sh"
waitSeconds = 60

def sendMail(message):
    msg = MIMEText(message)
    config = SafeConfigParser()
    config.read('restart_honeypot.conf')

    msg['Subject'] = 'Cowrie restart failed'
    me = config.get("mail", "address")
    you = me
    msg['From'] = me
    msg['To'] = you

    s = smtplib.SMTP(config.get("mail", "server"))
    s.sendmail(me, [you], msg.as_string())
    s.quit()

def pidAlive(pid):
    """ Check for the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True

logging.basicConfig(filename=logFile, level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(message)s [%(pathname)s:%(lineno)d]")

logging.info("Honeyport restarter invoked")

if os.path.isfile(cowriePidFile):
    try:
        with open(cowriePidFile) as pidFile:
            pid = int(pidFile.read().rstrip())
    except IOError:
        message = "Couldn't read cowrie pidfile"
        logging.exception(message)
        sendMail(message)
        sys.exit(2)
    else:
        logging.info("Restarting cowrie process %d, will wait for %d seconds for it to die", pid, waitSeconds)

        subprocess.call([stopScript], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        for sec in range(waitSeconds):
            time.sleep(1)
            if not pidAlive(pid):
                logging.info("Cowrie died gracefully after %d seconds", sec)
                break

        # we need SIGKILL if graceful shutdown wasn't enough
        if pidAlive(pid):
            try:
                logging.warn("PID %d still alive after graceful shutdown, going for SIGKILL", pid)
                os.kill(pid, signal.SIGKILL)
                time.sleep(2)
            except OSError:
                logging.exception("Kill failed, maybe it fizzled")
            if pidAlive(pid):
                message = "PID %d alive after SIGKILL, did it get stuck in kernel?" % pid
                logging.warn(message)
                sendMail(message)
                sys.exit(3)
else:
    logging.info("Cowrie not running, no need to stop it")

output = ""
try:
    output = subprocess.check_output([startScript], stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    logging.info("Start script called")
except:
    message = "Start script ended with nonzero status"
    logging.exception(message)
    sendMail(message)
    sys.exit(4)

if re.search('Traceback', output):
    message = "error occured, check Traceback"
    logging.exception(message)
    sendMail(message)
    sys.exit(5)


